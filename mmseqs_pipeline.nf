#!/usr/bin/env nextflow

/*
How to run:
nextflow run mmseqs_pipeline.nf
*/

/* Specificy directory where run pipeline*/
currDir = '<fill in path>'

/* Specificy input file to run pipeline on*/
input_file = '<input file name>'

params.in = "<fill in path>/${input_file}"
all_unannotated_reads_contigs = file(params.in)

process cluster_unknown_seqs{
  conda 'bioconda::mmseqs2=9.d36de-0'
  publishDir "${currDir}/mmseqs_clu"

  input:
  file 'all_unannotated_seqs.fa' from all_unannotated_reads_contigs

  output:
  file "all_unannotated_seqs_DB_clu_seq_3_250.fasta" into clusters_3_250
  file "all_unannotated_seqs_DB_clu.tsv" into all_clu

  script:
  """
  mmseqs createdb all_unannotated_seqs.fa all_unannotated_seqs_DB
  mmseqs cluster all_unannotated_seqs_DB all_unannotated_seqs_DB_clu tmp -a \
  --max-seqs 10000 -e 1e-5 --threads 16
  mmseqs createtsv all_unannotated_seqs_DB all_unannotated_seqs_DB \
  all_unannotated_seqs_DB_clu all_unannotated_seqs_DB_clu.tsv --threads 16
  mmseqs createseqfiledb all_unannotated_seqs_DB all_unannotated_seqs_DB_clu \
  all_unannotated_seqs_DB_clu_seq_3_250 --min-sequences 3 --max-sequences 250 \
  --threads 16
  mmseqs result2flat all_unannotated_seqs_DB all_unannotated_seqs_DB \
  all_unannotated_seqs_DB_clu_seq_3_250 all_unannotated_seqs_DB_clu_seq_3_250.fasta
  """
}

/* Split each cluster into its own fasta file */

process split_clu_into_indiv_fasta_files{
  publishDir "${currDir}/mmseqs_clu_3_250"

  input:
  file 'all_unannotated_seqs_DB_clu_seq_3_250.fasta' from clusters_3_250

  output:
  file "*.fa" into clu_files mode flatten

  script:
  """
  #!/usr/bin/env python
  import sys

  clu_3_250_file = 'all_unannotated_seqs_DB_clu_seq_3_250.fasta'
  clu_3_250_open = open(clu_3_250_file)

  prev_line = next(clu_3_250_open)
  curr_lines = []
  for curr_line in clu_3_250_open:
      if prev_line.startswith('>') and curr_line.startswith('>'):
          if len(curr_lines) > 0:
              with open(clu_rep + '.fa','w') as curr_clu_file:
                  curr_clu_file.writelines(curr_lines[:-1])
              curr_lines = []
          clu_rep = prev_line.strip('>')
          clu_rep = clu_rep.rstrip()
          curr_lines.append(curr_line)
      else:
          curr_lines.append(curr_line)
      prev_line = curr_line
  with open(clu_rep + '.fa','w') as curr_clu_file:
      curr_clu_file.writelines(curr_lines)
  """
}

clu_file = clu_files.map{file -> tuple(file.baseName, file)}
clu_file.into {clu_msa;
               compare_to_aln}

/* K-align to get MSA of each cluster */

process cluster_MSA_kalign{
  conda 'bioconda::kalign2'
  publishDir "${currDir}/mmseqs_kalign_msa"

  input:
  set fileID, file(cluster_file) from clu_msa

  output:
  file("${fileID}.fsa") into clu_kalign_msa_files mode flatten

  script:
  """
  kalign ${cluster_file} ${fileID}.fsa
  """
}

/* Extract percent identities from each cluster MSA */

alistat_in = clu_kalign_msa_files.map{file -> tuple(file.baseName, file)}

process get_percent_identities_from_MSA {
  publishDir "${currDir}/mmseqs_alistat_pident"

  input:
  set fileID, file(msa_file) from alistat_in

  output:
  file("${fileID}.txt") into pairwise_id_clu_files mode flatten

  script:
  """
  alistat --identmx ${fileID}.txt ${msa_file}
  """
}

/* Perform the modified single-linkage on each cluster */

/*
Subcluster each cluster for sequences with 95% or greater sequence identity
and chose the longest sequence as the representative. Replace the subclustered
sequences with the representative sequence in the cluster.
*/

pident_file = pairwise_id_clu_files.map{file -> tuple(file.baseName, file)}
combine_pident_clu = pident_file.join(compare_to_aln)

process subcluster_single_linkage{
  publishDir "${currDir}/mmseqs_subclu_clu"

  input:
  set fileID, file(pident_file), file(clu_file) from combine_pident_clu

  output:
  file("${fileID}.fasta") into new_clusters mode flatten

  script:
  """
  #!/usr/bin/env python
  import sys

  ident_file = '${pident_file}'
  clu_file = '${clu_file}'

  pair_dict = dict()
  with open(ident_file, 'r') as aln_open:
      for line in aln_open:
          items = line.split()
          query = items[2]
          target = items[3]
          pident = float(items[4])
          if query != target:
              pair = (query, target)
              pair_dict[pair] = pident

  len_dict = dict()
  with open(clu_file, 'r') as clu_open:
      for line in clu_open:
          if line.startswith('>'):
              line_items = line.split()
              seq_id = line_items[0]
              seq_id = seq_id.strip('>')
              length = line_items[2]
              length = int(length.strip('length='))
              len_dict[seq_id] = length

  seqid_to_subclu = dict()
  subclu_to_rep = dict()
  subclu_to_seqid = dict()
  num_of_subclu = 0

  for pair in pair_dict.keys():
    query = pair[0]
    target = pair[1]
    pident = pair_dict[pair]
    if pident >= 0.950:
        query_in_subclu = seqid_to_subclu.has_key(query)
        target_in_subclu = seqid_to_subclu.has_key(target)
        if query_in_subclu and target_in_subclu: #both are in a subclu
            query_subclu_num = seqid_to_subclu[query]
            target_subclu_num = seqid_to_subclu[target]
            if query_subclu_num != target_subclu_num: #in diff subclus
                query_subclu_rep = subclu_to_rep[query_subclu_num]
                target_subclu_rep = subclu_to_rep[target_subclu_num]
                if len_dict[query_subclu_rep] >= len_dict[target_subclu_rep]:
                    shorter_subclu_num = target_subclu_num
                    longer_subclu_num = query_subclu_num
                    new_rep_seq = query_subclu_rep
                if len_dict[target_subclu_rep] > len_dict[query_subclu_rep]:
                    shorter_subclu_num = query_subclu_num
                    longer_subclu_num = target_subclu_num
                    new_rep_seq = target_subclu_rep
                smaller_seq_subclu_seq_list = subclu_to_seqid[shorter_subclu_num]
                for seq in smaller_seq_subclu_seq_list:
                    seqid_to_subclu[seq] = longer_subclu_num
                subclu_to_rep.pop(shorter_subclu_num)
                subclu_to_rep[longer_subclu_num] = new_rep_seq
                subclu_to_seqid[longer_subclu_num] += smaller_seq_subclu_seq_list
                subclu_to_seqid.pop(shorter_subclu_num)
        elif query_in_subclu or target_in_subclu: #if either query or target already in subclu
            if query_in_subclu: #query already in subclu
                seq_already_in = query
                seq_not_in = target
            if target_in_subclu: #target already in subclu
                seq_already_in = target
                seq_not_in = query
            subclu_num = seqid_to_subclu[seq_already_in]
            subclu_rep = subclu_to_rep[subclu_num]
            rep_len = len_dict[subclu_rep]
            #Do I need these next two if statements?
            if len_dict[seq_already_in] >= len_dict[seq_not_in]:
                competing_seq_len = len_dict[seq_already_in]
                competing_seq = seq_already_in
            if len_dict[seq_not_in] > len_dict[seq_already_in]:
                competing_seq_len = len_dict[seq_not_in]
                competing_seq = seq_not_in
            if competing_seq_len > rep_len:
                subclu_to_rep[subclu_num] = competing_seq
            seqid_to_subclu[seq_not_in] = subclu_num
            subclu_to_seqid[subclu_num] += [seq_not_in]
        else: #neither is in a subclu
            query_len = len_dict[query]
            target_len = len_dict[target]
            num_of_subclu += 1
            subclu_to_seqid[num_of_subclu] = [query, target]
            seqid_to_subclu[query] = num_of_subclu
            seqid_to_subclu[target] = num_of_subclu
            if len_dict[query] >= len_dict[target]:
                subclu_to_rep[num_of_subclu] = query
            if len_dict[target] > len_dict[query]:
                subclu_to_rep[num_of_subclu] = target

  new_clu = dict()
  with open(clu_file, 'r') as clu_open:
      for line in clu_open:
          next_line = next(clu_open)
          if line.startswith('>'):
              line_items = line.split()
              seq_id = line_items[0]
              seq_id = seq_id.strip('>')
              if len(subclu_to_rep) != 0:
                  if seq_id in subclu_to_rep.values():
                      new_clu[line] = next_line
                  if seq_id not in seqid_to_subclu.keys():
                      new_clu[line] = next_line
              else:
                  new_clu[line] = next_line

  with open('${fileID}.fasta','w') as new_clu_file:
      for key,value in new_clu.items():
          new_clu_file.write("{}".format(key))
          new_clu_file.write("{}".format(value))

  """
}


subclu_clu_files = new_clusters.map{file -> tuple(file.baseName, file)}

/* Select clusters of size 8 or greater */

process size_select_new_clu{
  publishDir "${currDir}/mmseqs_size8_subclu_clu"

  input:
  set fileID, file(clu_file) from subclu_clu_files

  output:
  file("${fileID}_final.fa") optional true into size_selected_clu mode flatten

  script:
  """
  #!/usr/bin/env python

  with open('${clu_file}') as clu_file:
      num_seqs = sum('>' in line for line in clu_file)

  if num_seqs >=8:
      with open('${clu_file}') as clu_file:
          with open('${fileID}_final.fa', 'w') as size_sel_clu:
              for line in clu_file:
                  size_sel_clu.write(line)

  """
}

/* Generate a MSA of each cluster with MACSE (condon aware aligner) */

select_clu = size_selected_clu.map{file -> tuple(file.baseName, file)}

process macse_on_each_cluster{
  publishDir "${currDir}/mmseqs_macse_aln"

  input:
  set fileID, file(clu_file) from select_clu

  output:
  file("${fileID}_*.fasta") into macse_alignments mode flatten

  script:
  """
  java -jar -Xmx5000m $macse -i ${clu_file} -o ${fileID} -s -60 -f -10 -g -10
  """
}

/**
TODO: Discard low quality (poor overlap) alignments

TODO: calibrated RNAcode for short-read sequencing data
      (to get a more accurate p-value threshold)
**/

/* Parse MACSE output for input into RNAcode */

/*
process parse_macse_for_rnacode{
  publishDir "${currDir}/msa_files"

  input:
  set fileID, file(macse_file) from macse_file

  output:
  file("${fileID}.fsa") into edited_macse_files mode flatten
  file("${fileID}.aln") into clustalw_files mode flatten

  script:
  """
  #!/usr/bin/env python3
  from Bio import AlignIO

  with open('${macse_file}', 'r') as macse_file, open('${fileID}.fsa', 'w') as editing_macse:
      for line in macse_file:
          line = line.replace('!','-')
          editing_macse.write(line)

  AlignIO.convert('${fileID}.fsa', "fasta", '${fileID}.aln', "clustal")
  """
}

clustalw_file = clustalw_files.map{file -> tuple(file.baseName, file)}

/* Run RNAcode on each MACSE cluster alignment */

/*
process rnacode_predict_protein_families{
  conda 'bioconda::rnacode'
  publishDir "${currDir}/rnacode_files"

  input:
  set fileID, file(clustal_macse) from clustalw_file

  output:
  file("${fileID}.txt") into rnacode_output mode flatten
  //Write output for the eps directories

  script:
  """
  RNAcode --outfile ${fileID}.txt --eps --eps-cutoff 1.0 --eps-dir ${fileID}_eps \
  --tabular ${clustal_macse}
  """
}

*/

/**
TODO: RNAcode predictions with p-value threshold P <= 0.15 to remove non-coding
      clusters

TODO: MSA translated to amino acids according to frame prediction
      by RNAcode with CodonAlign

TODO: Seg to remove clusters with low complexity alignments
      (predicted amino acid sequences with more than 50% low complexity)

TODO: Composite score to rank protein families
**/
