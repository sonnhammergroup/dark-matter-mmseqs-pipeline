# Dark Matter MMSeqs Pipeline

This repository contains two versions of part of the viral discovery pipeline
described in Barrientos-Somarribas M, *et al*, 2018. Both versions contain the
following steps from the pipeline:

1. Clustering unknown viral sequences,
2. Modified single-linkage sub-clustering step,
3. Codon-aware multiple sequences alignments built from each cluster, and
4. Coding potential predictions for each cluster.

The main difference between the two versions is the clustering step. One
version does the clustering with mmseqs2. The other version attempts to
recreate the original work using blastall, mcxload, and mcl for the clustering.

Both versions are implemented in nextflow.

**Note:** The following instructions were only tested on octa4.

## Before running both versions

Install:

1. [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html)
2. [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
3. [Bioconda](https://bioconda.github.io/)
3. Python 2
4. Python 3
5. [biosquid](http://eddylab.org/software/squid/)
6. [MACSE release 0.8b2](https://bioweb.supagro.inra.fr/macse/index.php?menu=releases)

You need to first assign the correct macse java jar file to the environment
variable ```macse```. You can do this by running:

```bash
export macse="<path to macse root>/macse_v0.8b2.jar"
```
You need to add biosquid to your path for alistat.
You can do this by running:

```bash
export PATH="<path to biosquid root>/biosquid/bin/:$PATH"
```

Currently, the pipelines are set up such that you need to set certain directory
paths and file names. The places that need to be substituted are denoted
by < >'s. In the future, this is a good place to improve the automation further.

## Running the mmseqs2 version
To run the pipeline:
```bash
nextflow run mmseqs_pipeline.nf
```

To run with reports:
```bash
nextflow run mmseqs_pipeline.nf -with-report -with-trace -with-timeline -with-dag
```

## Running the mcl version
To run the pipeline:
```bash
nextflow run old_pipeline.nf
```

To run with reports:
```bash
nextflow run old_pipeline.nf -with-report -with-trace -with-timeline -with-dag
```

## Next steps
The bottom of both pipeline files contains a parsing step to parse the macse
fasta file output into the clustalw input format for RNAcode. The parsing step
is followed by a process containing the RNAcode call.

The TODOs in the pipeline files describe missing steps in the pipelines and
where they go.
