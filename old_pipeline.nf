#!/usr/bin/env nextflow

/*
How to run:
nextflow run old_pipeline.nf
*/

/* Specificy directory where run pipeline*/
currDir = '<fill in path>'

/* Specificy input file to run pipeline on*/
input_file = Channel.fromPath("<fill in path>")
input_file.into {all_unannotated_reads_contigs;
                    split_clu_in}

/* All-versus-all alignment with blstall */

process all_versus_all_alignment{
  conda 'bioconda::blast-legacy'
  publishDir "${currDir}/mcl_blastall_aln"

  input:
  file 'all_unannotated_seqs.fa' from all_unannotated_reads_contigs

  output:
  file "blast_tab.txt" into all_v_all_aln

  script:
  """
  formatdb -p F -i all_unannotated_seqs.fa -t myunknown_db -n myunknown_db -o T
  blastall -p blastn -d myunknown_db -i all_unannotated_seqs.fa -o blast_tab.txt \
  -e 1e-5 -m 9 -v 10000 -b 10000 -a 16 -K 100000
  """
}

/* Clustering with MCL */

process clu_preprocessing{
  conda 'bioconda::mcl'
  publishDir "${currDir}/mcl_preprocess_clu"

  input:
  file 'blast_tab.txt' from all_v_all_aln

  output:
  file "blast_tab.abc" into grep_preprocess
  file "blast_tab.mci" into mci_file
  file "blast_tab.tab" into tab_file

  script:
  """
  grep -v "#" -v blast_tab.txt | cut -f 1,2,11 > blast_tab.abc
  mcxload -abc blast_tab.abc --stream-mirror --stream-neg-log10 \
  -stream-tf 'ceil(200)' -o blast_tab.mci -write-tab blast_tab.tab
  """
}

process cluster_unknown_seqs{
  conda 'bioconda::mcl'
  publishDir "${currDir}/mcl_clu"

  input:
  file(mci) from mci_file
  file(tab) from tab_file

  output:
  file "out.blast_tab.mci.I60" into mcl_clu

  script:
  """
  mcl ${mci} -I 6 -scheme 7 -te 16 -use-tab ${tab} -o out.blast_tab.mci.I60
  """
}

/*Select clusters size 3-250, put each of these clusters into own fasta file,
which includes sequence information*/

process select_clu_size_3_250_and_split_each_into_own_fasta_file{
  publishDir "${currDir}/mcl_clu_3_250"

  input:
  file 'out.blast_tab.mci.I60' from mcl_clu
  file 'original_unknown_seqs' from split_clu_in

  output:
  file '*.fa' into mcl_clu_files mode flatten

  script:
  """
  #!/usr/bin/env python2

  import sys

  mcl_clu_file = 'out.blast_tab.mci.I60'
  org_fasta_file = 'original_unknown_seqs'

  mcl_clu = dict()
  with open(mcl_clu_file, 'r') as mcl_clu_open:
    for line in mcl_clu_open:
        line = line.rstrip()
        line_list = line.split()
        clu_rep = line_list[0]
        if clu_rep not in mcl_clu:
            mcl_clu[clu_rep] = []
            mcl_clu[clu_rep] = [clu_rep]
        for clu_memb in line_list[1:]:
            mcl_clu[clu_rep] += [clu_memb]

  mcl_clu_size = dict()
  for key,value in mcl_clu.items():
      mcl_clu_size[key] = int(len(value))

  mcl_clu_3_250_size = dict()
  for key,value in mcl_clu_size.items():
      if value >=3 and value <= 250:
          mcl_clu_3_250_size[key] = value

  keep_mcl_clu = dict()
  for key in mcl_clu_3_250_size.viewkeys() & mcl_clu.viewkeys():
      keep_mcl_clu[key] = mcl_clu[key]

  fasta_dict = dict()
  with open(org_fasta_file, 'r') as org_fasta_open:
      fasta_list = []
      for line in org_fasta_open:
          if line.startswith('>'):
              if len(fasta_list) > 0:
                  seq = ''.join(fasta_list[1:])
                  header = fasta_list[0]
                  header_list = header.split()
                  seq_id = header_list[0]
                  seq_id = seq_id.strip('>')
                  fasta_dict[seq_id] = [header, seq]
                  fasta_list = []
              line = line.rstrip()
              fasta_list.append(line)
          else:
              line = line.rstrip()
              fasta_list.append(line)
      seq = ''.join(fasta_list[1:])
      header = fasta_list[0]
      header_list = header.split()
      seq_id = header_list[0]
      seq_id = seq_id.strip('>')
      fasta_dict[seq_id] = [header, seq]

  for clu_rep, clu_membs in keep_mcl_clu.items():
      with open(clu_rep + '.fa', 'w') as clu_file:
          if clu_rep in fasta_dict:
              print >>clu_file, fasta_dict[clu_rep][0]
              print >>clu_file, fasta_dict[clu_rep][1]
          for memb in clu_membs:
              if memb in fasta_dict:
                  if memb != clu_rep:
                      print >>clu_file, fasta_dict[memb][0]
                      print >>clu_file, fasta_dict[memb][1]

  """
}

mcl_clu_file = mcl_clu_files.map{file -> tuple(file.baseName, file)}
mcl_clu_file.into {clu_msa;
                   mcl_clu;
                   pre_file}

/* Do a MSA of each cluster file using kalign */

process cluster_MSA_kalign{
  conda 'bioconda::kalign2'
  publishDir "${currDir}/mcl_kalign_msa"

  input:
  set fileID, file(cluster_file) from clu_msa

  output:
  file("${fileID}.fsa") into clu_kalign_msa_files mode flatten

  script:
  """
  kalign ${cluster_file} ${fileID}.fsa
  """
}

/* Extract percent identities from each cluster MSA */

alistat_in = clu_kalign_msa_files.map{file -> tuple(file.baseName, file)}

process get_percent_identities_from_MSA{
  publishDir "${currDir}/mcl_alistat_pident"

  input:
  set fileID, file(msa_file) from alistat_in

  output:
  file("${fileID}.txt") into pairwise_id_clu_files mode flatten

  script:
  """
  alistat --identmx ${fileID}.txt ${msa_file}
  """
}

pident_file = pairwise_id_clu_files.map{file -> tuple(file.baseName, file)}
combine_pident_clu = pident_file.join(mcl_clu)

/* Perform the modified single-linkage on each cluster */

/*
Subcluster each cluster for sequences with 95% or greater sequence identity
and chose the longest sequence as the representative. Replace the subclustered
sequences with the representative sequence in the cluster.
*/

process modified_single_linkage{
  publishDir "${currDir}/mcl_subclu_clu"

  input:
  set fileID, file(pident_file), file(clu_file) from combine_pident_clu

  output:
  file("${fileID}.fasta") into new_clusters mode flatten

  script:
  """
  #!/usr/bin/env python
  import sys

  ident_file = '${pident_file}'
  clu_file = '${clu_file}'

  pair_dict = dict()
  with open(ident_file, 'r') as aln_open:
      for line in aln_open:
          items = line.split()
          query = items[2]
          target = items[3]
          pident = float(items[4])
          if query != target:
              pair = (query, target)
              pair_dict[pair] = pident

  len_dict = dict()
  with open(clu_file, 'r') as clu_open:
      for line in clu_open:
          if line.startswith('>'):
              line_items = line.split()
              seq_id = line_items[0]
              seq_id = seq_id.strip('>')
              length = line_items[2]
              length = int(length.strip('length='))
              len_dict[seq_id] = length

  seqid_to_subclu = dict()
  subclu_to_rep = dict()
  subclu_to_seqid = dict()
  num_of_subclu = 0

  for pair in pair_dict.keys():
    query = pair[0]
    target = pair[1]
    pident = pair_dict[pair]
    if pident >= 0.950:
        query_in_subclu = seqid_to_subclu.has_key(query)
        target_in_subclu = seqid_to_subclu.has_key(target)
        if query_in_subclu and target_in_subclu: #both are in a subclu
            query_subclu_num = seqid_to_subclu[query]
            target_subclu_num = seqid_to_subclu[target]
            if query_subclu_num != target_subclu_num: #in diff subclus
                query_subclu_rep = subclu_to_rep[query_subclu_num]
                target_subclu_rep = subclu_to_rep[target_subclu_num]
                if len_dict[query_subclu_rep] >= len_dict[target_subclu_rep]:
                    shorter_subclu_num = target_subclu_num
                    longer_subclu_num = query_subclu_num
                    new_rep_seq = query_subclu_rep
                if len_dict[target_subclu_rep] > len_dict[query_subclu_rep]:
                    shorter_subclu_num = query_subclu_num
                    longer_subclu_num = target_subclu_num
                    new_rep_seq = target_subclu_rep
                smaller_seq_subclu_seq_list = subclu_to_seqid[shorter_subclu_num]
                for seq in smaller_seq_subclu_seq_list:
                    seqid_to_subclu[seq] = longer_subclu_num
                subclu_to_rep.pop(shorter_subclu_num)
                subclu_to_rep[longer_subclu_num] = new_rep_seq
                subclu_to_seqid[longer_subclu_num] += smaller_seq_subclu_seq_list
                subclu_to_seqid.pop(shorter_subclu_num)
        elif query_in_subclu or target_in_subclu: #if either query or target already in subclu
            if query_in_subclu: #query already in subclu
                seq_already_in = query
                seq_not_in = target
            if target_in_subclu: #target already in subclu
                seq_already_in = target
                seq_not_in = query
            subclu_num = seqid_to_subclu[seq_already_in]
            subclu_rep = subclu_to_rep[subclu_num]
            rep_len = len_dict[subclu_rep]
            #Do I need these next two if statements?
            if len_dict[seq_already_in] >= len_dict[seq_not_in]:
                competing_seq_len = len_dict[seq_already_in]
                competing_seq = seq_already_in
            if len_dict[seq_not_in] > len_dict[seq_already_in]:
                competing_seq_len = len_dict[seq_not_in]
                competing_seq = seq_not_in
            if competing_seq_len > rep_len:
                subclu_to_rep[subclu_num] = competing_seq
            seqid_to_subclu[seq_not_in] = subclu_num
            subclu_to_seqid[subclu_num] += [seq_not_in]
        else: #neither is in a subclu
            query_len = len_dict[query]
            target_len = len_dict[target]
            num_of_subclu += 1
            subclu_to_seqid[num_of_subclu] = [query, target]
            seqid_to_subclu[query] = num_of_subclu
            seqid_to_subclu[target] = num_of_subclu
            if len_dict[query] >= len_dict[target]:
                subclu_to_rep[num_of_subclu] = query
            if len_dict[target] > len_dict[query]:
                subclu_to_rep[num_of_subclu] = target

  new_clu = dict()
  with open(clu_file, 'r') as clu_open:
      for line in clu_open:
          next_line = next(clu_open)
          if line.startswith('>'):
              line_items = line.split()
              seq_id = line_items[0]
              seq_id = seq_id.strip('>')
              if len(subclu_to_rep) != 0:
                  if seq_id in subclu_to_rep.values():
                      new_clu[line] = next_line
                  if seq_id not in seqid_to_subclu.keys():
                      new_clu[line] = next_line
              else:
                  new_clu[line] = next_line

  with open('${fileID}.fasta','w') as new_clu_file:
      for key,value in new_clu.items():
          new_clu_file.write("{}".format(key))
          new_clu_file.write("{}".format(value))
  """
}


/* Select clusters of size 8 or greater */

new_clu_file = new_clusters.map{file -> tuple(file.baseName, file)}
new_clu_file.into {size_8_select;
                   post_file}

process size_select_new_clu{
  publishDir "${currDir}/mcl_size8_subclu_clu"

  input:
  set fileID, file(clu_file) from size_8_select

  output:
  file("${fileID}_final.fa") optional true into size_selected_clu mode flatten

  script:
  """
  #!/usr/bin/env python

  with open('${clu_file}') as clu_file:
      num_seqs = sum('>' in line for line in clu_file)

  if num_seqs >=8:
      with open('${clu_file}') as clu_file:
          with open('${fileID}_final.fa', 'w') as size_sel_clu:
              for line in clu_file:
                  size_sel_clu.write(line)

  """
}

/* Generate a MSA of each cluster with MACSE (condon aware aligner) */

size_8_clu = size_selected_clu.map{file -> tuple(file.baseName, file)}

process macse_on_each_cluster{
  publishDir "${currDir}/mcl_macse_aln"

  input:
  set fileID, file(clu_file) from size_8_clu

  output:
  file("${fileID}_*.fasta") into macse_alignments mode flatten

  script:
  """
  java -jar -Xmx5000m $macse -i ${clu_file} -o ${fileID} -s -60 -f -10 -g -10
  """
}

/**
TODO: Discard low quality (poor overlap) alignments

TODO: calibrated RNAcode for short-read sequencing data
      (to get a more accurate p-value threshold)
**/

/* Parse MACSE output for input into RNAcode */

/*
process parse_macse_for_rnacode{
  publishDir "${currDir}/msa_files"

  input:
  set fileID, file(macse_file) from macse_file

  output:
  file("${fileID}.fsa") into edited_macse_files mode flatten
  file("${fileID}.aln") into clustalw_files mode flatten

  script:
  """
  #!/usr/bin/env python3
  from Bio import AlignIO

  with open('${macse_file}', 'r') as macse_file, open('${fileID}.fsa', 'w') as editing_macse:
      for line in macse_file:
          line = line.replace('!','-')
          editing_macse.write(line)

  AlignIO.convert('${fileID}.fsa', "fasta", '${fileID}.aln', "clustal")
  """
}

clustalw_file = clustalw_files.map{file -> tuple(file.baseName, file)}

/* Run RNAcode on each MACSE cluster alignment */

/*
process rnacode_predict_protein_families{
  conda 'bioconda::rnacode'
  publishDir "${currDir}/rnacode_files"

  input:
  set fileID, file(clustal_macse) from clustalw_file

  output:
  file("${fileID}.txt") into rnacode_output mode flatten
  //Write output for the eps directories

  script:
  """
  RNAcode --outfile ${fileID}.txt --eps --eps-cutoff 1.0 --eps-dir ${fileID}_eps \
  --tabular ${clustal_macse}
  """
}

*/

/**
TODO: RNAcode predictions with p-value threshold P <= 0.15 to remove non-coding
      clusters

TODO: MSA translated to amino acids according to frame prediction
      by RNAcode with CodonAlign

TODO: Seg to remove clusters with low complexity alignments
      (predicted amino acid sequences with more than 50% low complexity)

TODO: Composite score to rank protein families
**/
